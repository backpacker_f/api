<?php
// заголовки 
header("Access-Control-Allow-Origin: http://authentication-jwt/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// файлы необходимые для соединения с БД
include_once '/var/www/internetlab.ru/data/www/test-api.internetlab.ru/api/config/database.php';
include_once '/var/www/internetlab.ru/data/www/test-api.internetlab.ru/api/objects/user.php';

// получаем соединение с базой данных
$database = new Database();
$db = $database->getConnection();

// создание объекта 'User'
$user = new User($db);

// получаем данные
$data = json_decode(file_get_contents("php://input"));

// устанавливаем значения
$user->email = $data->email;
$email_exists = $user->emailExists();

// существует ли электронная почта и соответствует ли пароль тому, что находится в базе данных
if ( $email_exists && password_verify($data->password, $user->password) ) {
    // Не получилось проверить авторизацию по сессии через postman
    session_start();
    $_SESSION['email'] = $user->email;
    setcookie('email', $user->email, time()+60*60*24*30);

    // код ответа
    http_response_code(200);

    // создание jwt

    echo json_encode(
        array(
            "message" => "Успешный вход в систему.",
            "email" => $_SESSION['email']
        )
    );

} else {

    // код ответа
    http_response_code(401);

    // сказать пользователю что войти не удалось
    echo json_encode(array("message" => "Ошибка входа."));
}
