<?php
// необходимые HTTP-заголовки
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json");

include_once '/var/www/internetlab.ru/data/www/test-api.internetlab.ru/api/config/database.php';
include_once '/var/www/internetlab.ru/data/www/test-api.internetlab.ru/api/objects/user.php';

// Запрос проходит для авторизованного пользователя
if(isset($_COOKIE['email']))    {

    // получаем соединение с базой данных
    $database = new Database();
    $db = $database->getConnection();

    // подготовка объекта
    $user = new User($db);

    // установим свойство ID записи для чтения
    $user->id = isset($_GET['id']) ? $_GET['id'] : die();

    $user->readOne();

    if ($user->email!=null) {

        // создание массива
        $user_arr = array(
            "id" =>  $user->id,
            "firstname" => $user->firstname,
            "lastname" => $user->lastname,
            "email" => $user->email,
            "created" => $user->created
        );

        // код ответа - 200 OK
        http_response_code(200);

        // вывод в формате json
        echo json_encode($user_arr);
    } else {
        // код ответа - 404 Не найдено
        http_response_code(404);

        // сообщим пользователю
        echo json_encode(array("message" => "Пользователь не существует."), JSON_UNESCAPED_UNICODE);
    }
} else {
    echo json_encode(array("message" => "Вы не прошли авторизацию."), JSON_UNESCAPED_UNICODE);

}