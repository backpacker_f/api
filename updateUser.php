<?php

// необходимые HTTP-заголовки
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// подключаем файл для работы с БД и объектом User
include_once '/var/www/internetlab.ru/data/www/test-api.internetlab.ru/api/config/database.php';
include_once '/var/www/internetlab.ru/data/www/test-api.internetlab.ru/api/objects/user.php';

// получаем соединение с базой данных
$database = new Database();
$db = $database->getConnection();

// подготовка объекта
$user = new User($db);

// получаем id для редактирования
$data = json_decode(file_get_contents("php://input"));

// убеждаемся, что данные не пусты
if (
    !empty($data->id) &&
    !empty($data->firstname) &&
    !empty($data->lastname) &&
    !empty($data->email) &&
    !empty($data->password)
) {

    // установим значения свойств
    $user->id = $data->id;
    $user->firstname = $data->firstname;
    $user->lastname = $data->lastname;
    $user->email = $data->email;
    $user->password = $data->password;

    // обновление
    if ($user->update()) {

        // установим код ответа - 200 ok
        http_response_code(200);

        // сообщим пользователю
        echo json_encode(array("message" => "Пользователь был обновлён."), JSON_UNESCAPED_UNICODE);
    } else {

        // код ответа - 503 Сервис не доступен
        http_response_code(503);

        // сообщение пользователю
        echo json_encode(array("message" => "Невозможно обновить пользователя."), JSON_UNESCAPED_UNICODE);
    }
} else {

    // установим код ответа - 400 неверный запрос
    http_response_code(400);

    // сообщим пользователю
    echo json_encode(array("message" => "Невозможно обновить пользователя. Данные неполные."), JSON_UNESCAPED_UNICODE);
}
