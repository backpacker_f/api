POST Создание пользователя:
http://test-api.internetlab.ru/api/createUser.php 
{
  "firstname" : "Andrey",
  "lastname" : "Fedorov",
  "email" : "email@internetlab.ru",
  "password" : "password"
}

  
POST Обновление пользователя: 
http://test-api.internetlab.ru/api/updateUser.php
  
{
  "id" : "10",
  "firstname" : "Andrey",
  "lastname" : "Fedorov",
  "email" : "support@internetlab.ru",
  "password" : "interlabs"
}

POST авторизация пользователя:
http://test-api.internetlab.ru/api/login.php
{
    "email" : "email@internetlab.ru",
    "password" : "password"
}

Только для авторизованных пользователей: 

  GET Информация о пользователе: 
  http://test-api.internetlab.ru/api/getUser.php?id=10

  POST Удаление пользователя:
  http://test-api.internetlab.ru/api/deleteUser.php
  {
      "id" : "10"
  }

